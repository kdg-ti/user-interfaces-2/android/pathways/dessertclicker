package com.example.android.dessertclicker

import android.app.Application
import timber.log.Timber

class LineNumberDebugTree : Timber.DebugTree() {
  override fun createStackElementTag(element: StackTraceElement): String {
    return "(${element.fileName}:${element.lineNumber})#${element.methodName}"
  }
}

class ClickerApplication: Application() {
  override fun onCreate() {
    super.onCreate()
    Timber.plant(LineNumberDebugTree())
  }
}